'use strict';

window._python_config = window._python_config || {}

window.python = { config: {} };

(function () {
	async function load_script(url) {
		return new Promise(function (resolve, reject) {
			var script = document.createElement("script")
			script.type = "text/javascript"
			script.onload = resolve
			script.onerror = reject
			document.head.appendChild(script)
			script.src = url
		})
	}

	function show_python_error(err) {
		console.error(`Python Error:
Traceback (most recent call last):
${err.traceback.map(s => `  File "${c.filename}", line ${c.lineno}`).join("\n")}
${err.tp$name}: ${Sk.ffi.remapToJs(err.args)}`)
	}

	function add_config_key(key, default_value) {
		window.python.config[key] = window._python_config[key] || default_value
	}

	function retab_code(code) {
		var lines = code.split(/\r?\n/)
		var begin_position = 0

		// Finds the position of the first non-whitespace character on the first non-whitespace line
		for (var i = 0; i < lines.length; i++) {
			if (lines[i].match(/[^\s]/)) {
				begin_position = lines[i].search(/\S/)
				break
			}
		}

		if (begin_position != -1) {
			lines = lines.map(l => l.slice(begin_position))
			code = lines.join('\n')
		}

		return code
	}

	async function run(code, name) {
		code = retab_code(code)

		return Sk.misceval.asyncToPromise(function () {
			return Sk.importMainWithBody(name, false, code, true)
		})
	}

	async function run_and_catch(code, name) {
		try {
			await run(code, name)
		} catch (e) {
			show_python_error(e);
		}
	}

	async function init() {
		Sk.externalLibraries = window.python.config.external_libraries

		Sk.configure({
			output: window.python.config.output_function,
			read: (x) => {
				if (Sk.builtinFiles === undefined || Sk.builtinFiles["files"][x] === undefined) {
					throw `File not found: '${x}'`
				}
				return Sk.builtinFiles["files"][x]
			}
		})

		var tags = Array.from(document.getElementsByTagName('script')).filter(e => e.type == window.python.config.mime_type)

		for (var t = 0; t < tags.length; t++) {
			if (tags[t].src) {
				var url = tags[t].src

				var response = await fetch(url)
				var body = await response.text()

				var name
				try {
					name = url.match(/([^\/]+)(?=\.\w+$)/)[0]
				} catch (e) {
					name = window.python.config.default_script_name
				}

				run_and_catch(body, name)

			} else {
				run_and_catch(tags[t].text, window.python.config.default_script_name)
			}
		}
	}

	async function main() {
		add_config_key('output_function', console.log) // Use if you want print to go to an DOM element or other sinks
		add_config_key('skulpt_source', "https://glcdn.githack.com/pythondude325/python-js/raw/master/skulpt.min.js")
		add_config_key('stdlib_source', "https://glcdn.githack.com/pythondude325/python-js/raw/master/skulpt-stdlib.js")
		add_config_key('mime_type', "text/python") // To use something like application/x-python instead of text/python
		add_config_key('default_script_name', "<stdin>")
		add_config_key('external_libraries', {}) // example value: {'<import name>': {path: '<path to js or py file>'}}

		try {
			await load_script(window.python.config.skulpt_source)
			await load_script(window.python.config.stdlib_source)
		} catch (e) {
			console.error("Could not load python runtime.")
			console.error(e)
		}

		try {
			await init()
		} catch (e) {
			console.error("Could not initialize python runtime.")
			console.error(e)
		}

		Object.assign(window.python, {retab_code, load_script, show_python_error, add_config_key, run, run_and_catch});
	}

	main()
})()
